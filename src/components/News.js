import React, { useState, useEffect, useRef } from "react";
import axios from "axios";

export default function News() {
    const [results, setResults] = useState([]);
    const [query, setQuery] = useState("reacthooks");
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);
    const searchInputRef = useRef();

    useEffect(() => {
        getResults();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const getResults = async () => {
        setLoading(true);
        try {
            const res = await axios.get(
                `http://hn.algolia.com/api/v1/search?query=${query}`
            );
            setResults(res.data.hits);
        } catch (err) {
            setError(err);
        }
        setLoading(false);
    };

    const handleSearch = (e) => {
        e.preventDefault();
        getResults();
    };

    const handleClearSearch = () => {
        setQuery("");
        searchInputRef.current.focus();
    };

    return (
        <>
            <form onSubmit={handleSearch}>
                <input
                    type="text"
                    onChange={(e) => setQuery(e.target.value)}
                    value={query}
                    ref={searchInputRef}
                />
                <button type="submit">Search</button>
                <button type="button" onClick={handleClearSearch}>
                    Clear
                </button>
            </form>
            {loading ? (
                <div>loading result...</div>
            ) : (
                <ul>
                    {results.map((result, i) => (
                        <li key={i}>
                            <a href={result.url}>{result.title}</a>
                        </li>
                    ))}
                </ul>
            )}
            {error && <div>{error.message}</div>}
        </>
    );
}
