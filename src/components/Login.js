import React, { useState } from "react";

export default function Login() {
	const [username, setUsername] = useState("");
	const [password, setPassword] = useState("");
	const [user, setUser] = useState(null);

	const handleSubmit = (e) => {
		e.preventDefault();
		const userData = {
			username,
			password,
		};
		setUser(userData);
	};
	return (
		<div
			style={{ textAlign: "center", backgroundColor: "grey", padding:'14px' }}
		>
			<h2>Login</h2>
			<form
				style={{
					display: "grid",
					alignItems: "center",
					justifyItems: "center",
				}}
				onSubmit={handleSubmit}
			>
				<div className="form-group">
					<input
						type="text"
						onChange={(event) => setUsername(event.target.value)}
						placeholder="Username"
						className="form-control"
						value={username}
					/>
				</div>
				<div className="form-group">
					<input
						type="password"
						placeholder="Password"
						onChange={(event) => setPassword(event.target.value)}
						className="form-control"
						value={password}
					/>
				</div>

				<button type="submit" className="btn btn-primary">
					Submit
				</button>
			</form>
			{user && JSON.stringify(user, null, 2)}
		</div>
	);
}
