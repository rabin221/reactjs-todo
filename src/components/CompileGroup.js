import React, { useReducer } from "react";
import ComponentC from "./GroupComponents/ComponentC";
import ComponentB from "./GroupComponents/ComponentB";
import ComponentA from "./GroupComponents/ComponentA";
// import CounterOne from "./components/CounterOne";
// import CounterTwo from "./components/CounterTwo";
// import Practicing from "./components/Practicing";
// import Login from "./components/Login";
// import Register from "./components/Register";
// import News from "./components/News";

export const CountContext = React.createContext();

const initialState = 0;
const reducer = (state, action) => {
    switch (action) {
        case "increment":
            return state + 1;
        case "decrement":
            return state - 1;
        case "reset":
            return initialState;
        default:
            return state;
    }
};

export default function CompileGroup() {
    const [count, dispatch] = useReducer(reducer, initialState);
    return (
        <CountContext.Provider
            value={{ countState: count, countDispatch: dispatch }}
        >
            <div style={{ textAlign: "center", padding: "12px" }}>
                {/*<Login /> */}
                {/*<Register /> */}
                {/*<News /> */}
                {/* <CounterOne /> */}
                {/* <CounterTwo /> */}
                Count = {count}
                <ComponentA />
                <ComponentB />
                <ComponentC />
            </div>
        </CountContext.Provider>
    );
}
