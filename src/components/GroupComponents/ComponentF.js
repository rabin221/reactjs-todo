import React, { useContext } from "react";
import { CountContext } from "../CompileGroup";

export default function ComponentF() {
    const { countDispatch } = useContext(CountContext);
    return (
        <div>
            ComponentF
            <button onClick={() => countDispatch("increment")}>
                Increment
            </button>
            <button onClick={() => countDispatch("decrement")}>
                Decrement
            </button>
            <button onClick={() => countDispatch("reset")}>Reset</button>
        </div>
    );
}
