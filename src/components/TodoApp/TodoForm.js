import React, { useState, useContext } from "react";
import { TaskContext } from "../../context/TaskContext";

function TodoForm() {
	const { dispatch } = useContext(TaskContext);
	const [value, setValue] = useState("");

	const handleChange = (e) => {
		setValue(e.target.value);
	};
	const handleSubmit = (e) => {
		e.preventDefault();
		if (!value) return;
		dispatch({
			type: "ADD_TODO",
			todo: { text: value },
		});
		setValue("");
	};
	return (
		<>
			<form onSubmit={handleSubmit} className="form-inline mb-4">
				<input
					type="text"
					onChange={handleChange}
					value={value}
					className="form-control"
				/>
				<button type="submit" className="btn btn-primary ml-2">
					Add Todo
				</button>
			</form>
		</>
	);
}

export default TodoForm;
