import React from "react";
import Items from "./Items";
import TodoForm from "./TodoForm";

function Todo() {
	return (
		<div
			style={{
				display: "flex",
				alignItems: "center",
				justifyContent: "center",
				flexDirection: "column",
			}}
		>
			<h2>Todo List</h2>
			<TodoForm />
			<Items />
		</div>
	);
}

export default Todo;
