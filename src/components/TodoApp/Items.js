import React, { useContext } from "react";
import { FaTrash } from "react-icons/fa";
import { TaskContext } from "../../context/TaskContext";

function Items() {
	const { todos, dispatch } = useContext(TaskContext);
	return (
		<>
			<ul className="list-group">
				{todos &&
					todos.map((todo) => (
						<li
							key={todo.id}
							className="list-group-item d-flex justify-content-between"
						>
							<p className="mr-5">
								{todo.isCompleted ? (
									<del>{todo.text}</del>
								) : (
									todo.text
								)}
							</p>
							<div>
								<button
									onClick={() =>
										dispatch({
											type: "TOGGLE_COMPLETE",
											id: todo.id,
										})
									}
									className="btn btn-info"
								>
									Task Update
								</button>
								<button
									onClick={() =>
										dispatch({
											type: "REMOVE_TODO",
											id: todo.id,
										})
									}
									className="btn btn-danger"
								>
									<FaTrash />
								</button>
							</div>
						</li>
					))}
			</ul>
		</>
	);
}

export default Items;
