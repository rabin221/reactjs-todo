import React, { useState, useEffect } from "react";
import "./practice.css";

const initialLocationState = {
    latitude: null,
    longitude: null,
    speed: null,
};

export default function Practicing() {
    const [count, setCount] = useState(0);
    const [isOn, setIsOn] = useState(false);
    const [mouseMovement, setMouseMovement] = useState({ x: null, y: null });
    const [location, setLocation] = useState(initialLocationState);
    let mounted = true;

    useEffect(() => {
        window.addEventListener("mousemove", handleMouseMovement);
        navigator.geolocation.getCurrentPosition(handleGeolocation);
        return () => {
            window.removeEventListener("mousemove", handleMouseMovement);
            mounted = false;
        };
    }, []);

    const handleGeolocation = (event) => {
        if (mounted) {
            setLocation({
                latitude: event.coords.latitude,
                longitude: event.coords.longitude,
                speed: event.coords.speed,
            });
        }
    };
    const handleMouseMovement = (event) => {
        setMouseMovement({
            x: event.pageX,
            y: event.pageY,
        });
    };
    const handleClick = () => {
        setCount(count + 1);
    };
    const handleToggle = () => {
        setIsOn(!isOn);
    };
    return (
        <div className="practice">
            <h1>Count Number</h1>
            <button onClick={handleClick}>You clicked {count} times</button>

            <h1>Toggle</h1>
            <div
                style={{
                    width: "50px",
                    height: "50px",
                    backgroundColor: isOn ? "yellow" : "grey",
                }}
                onClick={handleToggle}
            />

            <h5>Mouse Direction</h5>
            <p>X-axix= {mouseMovement.x}</p>
            <p>Y-axis= {mouseMovement.y}</p>

            <h5>Geo Location</h5>
            <p>Latitude is {location.latitude}</p>
            <p>Longitude is {location.longitude}</p>
            <p>Your speed is {location.speed ? location.speed : "0"}</p>
        </div>
    );
}
