import React, { useState, useEffect } from "react";
import axios from "axios";

function DataFetchingOne() {
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);
    const [post, setPost] = useState({});

    useEffect(() => {
        const getResult = async () => {
            setLoading(true);
            await axios
                .get(`https://jsonplaceholder.typicode.com/posts/1`)
                .then((res) => {
                    console.log(res.data);
                    setPost(res.data);
                })
                .catch((err) => {
                    setError(err);
                });
            setLoading(false);
        };
        getResult();
    }, []);
    return (
        <div>
            {loading ? "Loading ..." : post.title}
            {error && <div>{error.message}</div>}
        </div>
    );
}

export default DataFetchingOne;
