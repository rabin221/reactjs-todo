import { v4 as uuid } from "uuid";

export const initialState = [
	{ id: uuid(), text: "Learn about React", isCompleted: false },
	{ id: uuid(), text: "Meet friend for lunch", isCompleted: true },
	{ id: uuid(), text: "Build really cool todo app", isCompleted: false },
];

export const todoReducer = (state, action) => {
	switch (action.type) {
		case "ADD_TODO":
			return [
				...state,
				{ id: uuid(), text: action.todo.text, isCompleted: false },
			];
		case "REMOVE_TODO":
			return state.filter((todos) => todos.id !== action.id);
		case "TOGGLE_COMPLETE":
			return state.map((yoyo) => {
				if (yoyo.id === action.id) {
					return { ...yoyo, isCompleted: !yoyo.isCompleted };
				}
				return yoyo;
			});
		// return state.map((todo) =>
		// 	todo.id === action.id
		// 		? { ...todo, isCompleted: !todo.isCompleted }
		// 		: todo
		// );
		default:
			return state;
	}
};
