import React from "react";
// import CompileGroup from "./components/CompileGroup";
// import DataFetchingOne from "./components/DataFetch/DataFetchingOne";
// import DataFetchingTwo from "./components/DataFetch/DataFetchingTwo";
// import Todo from "./components/TodoApp/Todo";
// import TaskContextProvider from "./context/TaskContext";
import { Route } from "react-router-dom";
import Login from "./components/Login";

function App() {
	return (
		<div>
			<Route path="/" exact={true} component={Login} />

			{/* <CompileGroup /> */}
			{/* <DataFetchingOne /> */}
			{/* <DataFetchingTwo /> */}
			{/* <TaskContextProvider>
				<Todo />
			</TaskContextProvider> */}
		</div>
	);
}

export default App;
