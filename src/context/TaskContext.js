import React, { createContext, useReducer } from "react";
import { todoReducer, initialState } from "../reducers/todoReducer";

export const TaskContext = createContext();

const TaskContextProvider = (props) => {
	const [todos, dispatch] = useReducer(todoReducer, initialState);

	// function passes to form and get data
	// const addTodo = (text) => {
	// 	const newTodo = [...todos, { id: uuid(), text, isCompleted: false }];
	// 	setTodos(newTodo);
	// };

	// const handleDelete = (id) => {
	// 	const isNotId = (todos) => todos.id !== id;
	// 	const newTodo = todos.filter(isNotId);
	// 	setTodos(newTodo);
	// };
	// const handleComplete = (id) => {
	// 	const newTodo = todos.map((yoyo) => {
	// 		if (yoyo.id === id) {
	// 			yoyo.isCompleted = !yoyo.isCompleted;
	// 		}
	// 		return yoyo;
	// 	});
	// 	setTodos(newTodo);
	// };
	return (
		<TaskContext.Provider value={{ todos, dispatch }}>
			{props.children}
		</TaskContext.Provider>
	);
};

export default TaskContextProvider;
